from django.shortcuts import render
from django.http import HttpResponseRedirect
from app_forum.models import Forum
# Create your views here.
response = {}
def index(request):
    html = 'app_home/app_home.html'
    forum = Forum.objects.all()
    forum_list = forum[::-1]
    response['forum_list'] = forum_list
    return render(request, html, response)
