from django.db import models

# Create your models here.
class Forum(models.Model):
    company_id = models.IntegerField(default=0)
    lowongan = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
