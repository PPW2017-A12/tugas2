from app_forum.models import Forum
from django.db import models

# Create your models here.
class Reply(models.Model):
	forum = models.ForeignKey(Forum, related_name='replies')
	user_id = models.CharField(max_length=50)
	reply_text = models.TextField()
	created_date = models.DateTimeField(auto_now_add=True)