from django.conf.urls import url
from .views import create_reply

urlpatterns = [
    url(r'^$', create_reply, name='create_reply'),
]
