from django.test import TestCase
from django.test import Client
from app_forum.models import Forum
from .models import Reply

# Create your tests here.
class AppReplyUnitTest(TestCase):
    def test_add_reply(self):
        Forum.objects.create(company_id=123, lowongan="lowongan dummy")
        response_post = Client().post('/reply/', {'forum_id': "1", 'user_id': "123", 'reply_text': "reply terhadap lowongan"})
        self.assertEqual(Reply.objects.count(), 1)