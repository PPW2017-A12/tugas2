from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Reply
from app_forum.models import Forum

# Create your views here.
@csrf_exempt
def create_reply(request):
	if request.method == 'POST':
		forum = Forum.objects.get(id=request.POST['forum_id'])
		reply = Reply.objects.create(
			forum=forum,
			user_id=request.POST['user_id'],
			reply_text=request.POST['reply_text']
		)

		return JsonResponse({
				'id': reply.id,
				'forum_id': reply.forum.id,
				'user_id': reply.user_id,
				'reply_text': reply.reply_text,
				'created_date': reply.created_date
			})