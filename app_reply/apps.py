from django.apps import AppConfig


class AppReplyConfig(AppConfig):
    name = 'app_reply'
