"""tugas2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import app_forum.urls as app_forum
import app_home.urls as app_home
import app_profile.urls as app_profile
import app_reply.urls as app_reply
from django.views.generic.base import RedirectView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^forum/', include(app_forum,namespace='app_forum')),
    url(r'^home/', include(app_home,namespace='app_home')),
    url(r'^profile/', include(app_profile,namespace='app_profile')),
    url(r'^reply/', include(app_reply,namespace='app_reply')),
    url(r'^$', RedirectView.as_view(url='/home/', permanent='true'), name='index'),
]
